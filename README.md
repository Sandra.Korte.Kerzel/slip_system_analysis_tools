# Slip_system_analysis_tools

A collection of tools for slip system analysis.
Please cite our review paper and tutorial if you use the code provided in this project. 

Gibson, J.S.K.-L.; Pei, R.; Heller, M.; Medghalchi, S.; Luo,W.; Korte-Kerzel, S. 
**Finding and Characterising Active Slip Systems: A Short Review and Tutorial with Automation Tools**
Materials 2021, 14, 407

**https://doi.org/10.3390/ma14020407**


# Micropillar_Visulisation
This folder contains the MATLAB scripts required to form a slipped micropillar model in order to check how specific slip systems manifest in a micropiilar.

Download them all and run Micropillar_SlipPlane.m - instructions for use are included in the top comments.

This project requires the [MTEX toolbox](https://mtex-toolbox.github.io) in order to function. This must be downloaded separately and loaded at the start of the analysis (line 30).

This code was developed using MatLab 2018b and MTEX-5.1.1.

# Slip_Line_Analysis
This folder contains the MATLAB scripts required to perform the automated slip line analysis.

Download them all and run SlipLines_main.m - instructions for use are included in the top comments.

This project requires the [MTEX toolbox](https://mtex-toolbox.github.io) in order to function. This must be downloaded separately and loaded at the start of the analysis (line 17).

This code was developed using MatLab 2018b and MTEX-5.3.1.

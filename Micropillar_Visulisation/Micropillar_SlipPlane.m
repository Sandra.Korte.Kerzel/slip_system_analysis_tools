% Script for analysing micropillar (KM, CZ, MH)

% Input: symmetry group (Lines 48/51) and orientation (line 54) 

% Output: Image of the slipped pillar 


%Instruction: 
% 1.) Fill in symmetry group (line 48/51)
% 2.) Define orientation (line 54). Can be used either with (hkil)[uvtw] or euler
% angles. For euler angles: "angle1*degree, angle2*degree, angle3*degree,"
% 3.) Select the slip system (lines 62 - 63):
%       j: select plane family (100, 110 or 111)
%       slipDirCount: change to select different crystallographic equivalent planes (select fromm all sS of family j)
% 4.) Set view angle (line 251) according to the rotation from the FIB/SEM. Check again for
% the correct notation of the angle (positive/negative). An angle of 0 �
% results in the x-axis to the right and the y-axis to the top. 

% This script is now setup for OIM. In line 129 an angle for an rotation
% between the SE and EBSD data is defined. This angle might change
% depending on the software or version number. When different software is
% used this angle should be checked.


%% load toolbox

clear
close all

addpath D:\sciebo\Simulation\mtex-5.1.1

startup_mtex

%% load library

scriptFile = mfilename('fullpath');
scriptFolder = scriptFile(1:end-numel(mfilename));

warning off;
addpath ([scriptFolder,'\exportFig']);
addpath ([scriptFolder,'\paruly']);
addpath ([scriptFolder,'\plane_line_intersect']);


%% Definitions

%CS symmetry
CS=loadCIF('FeSi');

% specimen symmetry
SS = specimenSymmetry('-1');

% define orientations
o = orientation('euler',310*degree,9*degree,42*degree,CS);

% define Pillar name
name='Pillar1';

%% Select SlipPlane

% j==1 (011) %j==2 (211) % j==3 (321)
j = 3; 
SlipCount = 16;

%Height to Diameter Ratio
DH=2; 

sSAll=slipSystem.bcc(CS);
sS=sSAll(j);
sS=sS.symmetrise;
slipPlaneAll=sS.n; 
slipDirAll=sS.b;

slipDir = slipDirAll(SlipCount);
slipPlane = slipPlaneAll(SlipCount);

%% Pillar

alpha = 0.7;
resu = 100;


gridResu = 0.01;
% slip magnitude and direction
offSetMag = -0.1; 
offsetZ = 0;
col = [0.0 0.5 0.8];
colorTop = [1.0 0.5 0.0];
ViewAngle = 45;

[x,y,z] = cylinder([1 1],resu);
z = z.*2*DH;
z = z-DH;

[xData, yData, zData] = prepareSurfaceData( x, y, z );

% Set up fittype and options.
ft = fittype( 'poly22' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Robust = 'Bisquare';

% Fit model to data.
[fitresult, gof] = fit( [xData, yData], zData, ft, opts );

cf=fitresult;
s = str2sym(formula( cf ));
cn = coeffnames( cf );
cv = coeffvalues( cf );
for i = 1:length( cv )
    s = subs( s, cn{i}, cv(i) );
end

[xp,yp] = meshgrid(-1:gridResu:1,-1:gridResu:1);

g = matlabFunction(s);
zp=g(xp,yp);

cyl=surf2patch(x,y,z);

nums = numel(cyl.vertices(cyl.vertices(:,3)<0,:))/3;

hBottom=patch('Faces',[1:nums],...
    'Vertices',cyl.vertices(cyl.vertices(:,3)<0,:));

hBottom.FaceVertexCData = repmat(col,[nums 1]);
hBottom.FaceAlpha = alpha;

% oim convention
rotRD = rotation('axis',zvector,'angle',-90*degree);

%%

slipDirPillar = (rotRD*o)*slipDir;

planeFunc = plotPlane((rotRD*o)*slipPlane,'noplot');

coeff = fliplr(coeffs(vpa(planeFunc)));

t=(rotRD*o)*slipPlane;
plane = [coeff, 0];

% Use X,Y,Z and the plane to compute the signed distance.
d = xp*plane(1) + yp*plane(2) + zp*plane(3) - plane(4);

% Use contour to get the ContourMatrix.
hold on

[~,hcontour] = contour(xp,yp,d,'LevelList',0);
c = get(hcontour,'ContourMatrix');
delete(hcontour);

% Loop through the ContourMatrix
c=c(:,c(2,:)<=1);
x2=c(1,:);
y2=c(2,:);
z2 = interp2(xp,yp,zp, x2,y2);

xx=vertcat(x(:),x2(:));
yy=vertcat(y(:),y2(:));
zz=vertcat(z(:),z2(:));

sideCheck=plane(1)*xx + plane(2)*yy + plane(3)*zz;

xxo=xx(sideCheck < 0);
yyo=yy(sideCheck < 0);
zzo=zz(sideCheck < 0);

xxu=xx(sideCheck > 0);
yyu=yy(sideCheck > 0);
zzu=zz(sideCheck > 0);


%% filter non display part

indzo = ((zzo >= min(z(:))) .* (zzo <= max(z(:)))) .* ((yyo >= min(y(:)))...
    .* (yyo <= max(y(:)))) .*((xxo >= min(x(:))) .* (xxo <= max(x(:))));

xxo=xxo(logical(indzo));
yyo=yyo(logical(indzo));
zzo=zzo(logical(indzo));

indzu = ((zzu >= min(z(:))) .* (zzu <= max(z(:)))) .* ((yyu >= min(y(:)))...
    .* (yyu <= max(y(:)))) .*((xxu >= min(x(:))) .* (xxu <= max(x(:))));

xxu=xxu(logical(indzu));
yyu=yyu(logical(indzu));
zzu=zzu(logical(indzu));

%%

DT = DelaunayTri([xxo,yyo,zzo]); 
DTu = DelaunayTri([xxu,yyu,zzu]); 

hullFacets = convexHull(DT); 
hullFacetsu = convexHull(DTu); 

offsetSlip = [slipDirPillar.x slipDirPillar.y slipDirPillar.z];
offsetSlip=(offsetSlip/norm(offsetSlip))*offSetMag;

hTopCut=trisurf(hullFacets,DT.X(:,1)+offsetSlip(1),DT.X(:,2)+...
    offsetSlip(2),DT.X(:,3)+offsetSlip(3)+offsetZ,...
    'FaceColor',colorTop);

set(hTopCut,'FaceVertexCData',repmat(colorTop,[numel(get(hTopCut,'Faces'))/3 1]))
set(hTopCut,'FaceAlpha',alpha)


hBtCut=trisurf(hullFacetsu,DTu.X(:,1),DTu.X(:,2),...
    DTu.X(:,3),'FaceColor',col);


set(hBtCut,'FaceVertexCData',repmat(col,[numel(get(hBtCut,'Faces'))/3 1]))
set(hBtCut,'FaceAlpha',alpha)

shading flat;

axis equal;

camlight('headlight');
light 

camlight
camlight('right');


% material shiny
material([0.4 0.5 0.7 1.5 1]);

%% plot the slip direction

plotVec(slipDirPillar,'length',1,'color','k','linewidth',0.6);

box off
axis off

return;

%save
box on
set(gca,'ytick',[]);
set(gca,'xtick',[]);
set(gca,'ztick',[]);



%% view

% from top clockwise is positive

% change angle to SE viewpoint of the Pillar relative to your EBSD map
tra=45; 

% adding 180� to top rotation - depends on your EBSD setup (chose slip direction accordingly)
% top view
view(180,90);
view(tra+180,90);

% 45� side view
view(tra+180,45); 

% 90� side view
view(tra+180,0);

% top view export
view(180,90);
export_fig([name,char(slipDir),'_top','.png'],...
     '-m5','-transparent');

% 45� view export
view(tra+180,45);
export_fig([name,char(slipPlane),'_',char(slipDir),'.png'],...
     '-m5','-transparent');
 
 % 90� view export
view(tra+180,0);
export_fig([name,char(slipDir),'_sideview','.png'],...
      '-m5','-transparent');


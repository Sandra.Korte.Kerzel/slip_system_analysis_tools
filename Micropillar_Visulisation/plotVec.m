function plotVec(v,varargin)

v = vector3d(v);
v_xyz = [v.x,v.y,v.z];
% arrow3d([0 0 0],...
%     v_xyz./norm(v_xyz).*[1 1 1]*get_option(varargin,'length',5),2,...
%     'line',.1)

if ~check_option(varargin,'nonorm')
    vPlot = v_xyz./norm(v_xyz);
else
    vPlot = v_xyz;
end

zeroP = get_option(varargin,'zero',[0 0 0]);
h = mArrow3(zeroP,zeroP + vPlot.*[1 1 1]*...
    get_option(varargin,'length',5), 'facealpha', 0.5, 'color',...
    get_option(varargin,'color','k'), 'stemWidth',...
    0.02*(get_option(varargin,'linewidth',1))); 

xyzPosi = zeroP + vPlot.*[1 1 1]*...
    get_option(varargin,'length',5)*1.03;

if check_option(varargin,'text')
    text(xyzPosi(1),xyzPosi(2),xyzPosi(3),...
        get_option(varargin,'text','text'));
end

% hh = findobj('type','line');
%     set(hh(end),'color',get_option(varargin,'color','k'))
%     
% set(hh(end),'linewidth',1.2);
function [planefunction] = plotPlane(mT,varargin)


xyC = get_option(varargin,'zero',[0 0 0]);

dimsBox = get_option(varargin,'extent',[14 10 6]);
xD = dimsBox(1);
yD = dimsBox(2); 
zD = dimsBox(3); 

pScale1 = get_option(varargin,'scalex',2);
pScale2 = get_option(varargin,'scaley',2);

slipPlane = mT;

vec = [slipPlane.x,slipPlane.y,slipPlane.z];

syms x y z;
P=[x, y, z];

if abs(vec(3)) < 1e-8; vec(3) = 1e-6; end;
if abs(vec(2)) < 1e-8; vec(2) = 1e-6; end;
if abs(vec(1)) < 1e-8; vec(1) = 1e-6; end;

planefunction = dot(vec, P);

if check_option(varargin,'noplot')
    return;
end

ezsurf(solve(planefunction,z),...
    [xyC(1)-xD/pScale1,xyC(1)+xD/pScale1,...
    xyC(2)-xD/pScale2,xyC(2)+xD/pScale2],...
    get_option(varargin,'grid',2))

hold on

pH = findobj(gca,'type','surface');
set(pH(1), 'FaceColor',get_option(varargin,'color','r'),...
    'FaceAlpha',get_option(varargin,'alpha',0.5),...
    'EdgeAlpha',1,'EdgeColor',get_option(varargin,'edgecolor','none'),...
    'MarkerEdgeColor','k','EdgeLighting','flat',...
    'AmbientStrength',0.9,'FaceLighting','gouraud',...
    'SpecularStrength',.7,'DiffuseStrength',.8)

if check_option(varargin,'interp')
    shading interp;
    cm = colormap(jet(256));

    if check_option(varargin,'blue')
        %cmp = cm(40:160,:);
        cmp = cm(100:150,:);
    else
        cmp = cm;
    end

    colormap (cmp);
end

grid on

box on
axis on
view([39 44])
axis equal

xlim([-dimsBox(1)/2 +dimsBox(1)/2])
ylim([-dimsBox(2)/2 +dimsBox(2)/2])
zlim([-dimsBox(3)/2 +dimsBox(3)/2])

set (gca,'XTickLabel','');
set (gca,'YTickLabel','');
set (gca,'ZTickLabel','');
set(gca, 'TickDir', 'in');
set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'ztick',[]);
grid off;
title('');
set(gcf,'color','w')
set(gca,'ytick',[])
set(gca,'ztick',[])

set(gca,'clipping','on');

if ~check_option(varargin,'interp') && check_option(varargin,'light') 

    light('Position',[0 0 dimsBox(3)*3],...
        'Style','local');
    light('Position',[+dimsBox(1)/2 +dimsBox(2)/2 dimsBox(3)*5],...
        'Style','local');
end

if ~check_option(varargin,'noaxes')

    plotVec(xvector,'zero',[-dimsBox(1)/2 -dimsBox(2)/2 -dimsBox(3)/2],...
        'linewidth',2.5);
    plotVec(yvector,'zero',[-dimsBox(1)/2 -dimsBox(2)/2 -dimsBox(3)/2],...
        'linewidth',2.5)
    plotVec(zvector,'zero',[-dimsBox(1)/2 -dimsBox(2)/2 -dimsBox(3)/2],...
        'linewidth',2.5)
end
